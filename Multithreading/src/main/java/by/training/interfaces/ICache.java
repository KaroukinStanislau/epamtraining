package by.training.interfaces;

import java.util.Map;

public interface ICache<K, V> {
    Map<?,?> getMap();
    void put(K key, V value);
    V get(K key);
}
