package by.training.impl;

import by.training.interfaces.ICache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrencyLFUCache<K, V> implements ICache<K, V> {
    private Map<K, Entry<V>> map;
    private int size;
    private int maxFrequency;
    private ReadWriteLock lock;

    public ConcurrencyLFUCache(final int size) {
        this.size = size;
        maxFrequency = size - 1;
        map = new ConcurrentHashMap<K, Entry<V>>(size);
        lock = new ReentrantReadWriteLock(true);
    }

    public Map<?, ?> getMap() {
        return map;
    }

    public void put(K key, V value) {
        lock.writeLock().lock();
        Entry<V> temp;
        try {
            temp = map.get(key);
            if (temp == null) {
                if (isFull()) {
                    removeEldestEntry();
                }
                temp = new Entry<V>(value, 0);
                map.put(key, temp);
            }
        } finally {
            lock.writeLock().unlock();
        }
    }

    public V get(Object key) {
        lock.readLock().lock();
        Entry<V> temp;
        try {
            temp = map.get(key);
            if (temp == null) {
                //System.out.println("Key: " + key + " not exists!");
                return null;
            }
            if (temp.frequency < maxFrequency) {
                temp.frequency++;
            }
            temp = map.get(key);
        } finally {
            lock.readLock().unlock();
        }
        return temp.value;
    }

    private void removeEldestEntry() {
        int minFrequency = Integer.MAX_VALUE;
        K key = null;
        for (Map.Entry<K, Entry<V>> entry : map.entrySet()) {
            Entry<V> temp = entry.getValue();
            if (temp.frequency < minFrequency) {
                key = entry.getKey();
                minFrequency = temp.frequency;
            }
        }
        //System.out.println("removed from LFU: " + map.get(key));
        map.remove(key);
    }

    private boolean isFull() {
        return map.size() == size;
    }

    private class Entry<V> {
        public V value;
        public int frequency;

        public Entry(V value, int frequency) {
            this.value = value;
            this.frequency = frequency;
        }

        @Override
        public String toString() {
            return "{" +
                    "value=" + value +
                    ", frequency=" + frequency +
                    "}";
        }
    }
}
