package by.training.impl;

import by.training.interfaces.ICache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrencyLRUCache<K, V> implements ICache<K, V> {

    private final int size;
    private ConcurrentLinkedQueue<K> linkedQueue;
    private ConcurrentHashMap<K, V> map;
    private ReadWriteLock lock;
    private ReentrantLock lockall;

    public ConcurrencyLRUCache(final int size) {
        this.size = size;
        this.linkedQueue = new ConcurrentLinkedQueue<K>();
        this.map = new ConcurrentHashMap<K, V>(size);
        lock = new ReentrantReadWriteLock(true);
        lockall = new ReentrantLock(true);
    }

    public V get(final K key) {
       lock.readLock().lock();
        V temp;
        try {
            if (!map.containsKey(key)) {
                //System.out.println("Key: " + key + " not exists!");
                return null;
            } else {
                //i think only one thread can enter in this section
                //I tested, and some times linkedQueue.size > map.size
                lockall.lock();

                linkedQueue.remove(key);
                linkedQueue.add(key);
                //section for test without lockall.lock()
                /*if (linkedQueue.size() != map.size()){
                    System.out.println("!!!!!");
                }*/
                lockall.unlock();
            }
            temp = map.get(key);
        } finally {
            lock.readLock().unlock();
        }
        return temp;
    }

    public void put(final K key, final V value) {
       lock.writeLock().lock();
        try {
            if (map.containsKey(key)) {
                linkedQueue.remove(key);
            }
            while (linkedQueue.size() >= size) {
                K oldestKey = linkedQueue.poll();
                if (oldestKey != null) {
                    /*System.out.println("removed from ConcurrencyLRU {value=" + map.get(oldestKey)
                            + "}");*/
                    map.remove(oldestKey);
                }
            }
            linkedQueue.add(key);
            map.put(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Map<K, V> getMap() {
        return map;
    }
}
