package by.training;

import by.training.impl.ConcurrencyLFUCache;
import by.training.impl.ConcurrencyLRUCache;
import by.training.interfaces.ICache;

public class Runner {
    private static final int CACHE_SIZE = 3;
    private static final int THREADS = 10000;

    public static void main(String[] args) {
        ICache cache = new ConcurrencyLRUCache(CACHE_SIZE);
        for (int i = 0; i < THREADS; i++) {
            Thread t = new Thread(new someWork(cache));
            t.start();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(cache.getMap());


        cache = new ConcurrencyLFUCache(CACHE_SIZE);
        for (int i = 0; i < THREADS; i++) {
            Thread t = new Thread(new someWork(cache));
            t.start();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(cache.getMap());
    }

    static class someWork implements Runnable {
        ICache cache;

        public someWork(ICache cache) {
            this.cache = cache;
        }

        public void run() {
            cache.put(1, 1);
            cache.put(2, 2);
            cache.get(1);
            cache.get(1);
            cache.get(1);
            cache.get(1);
            cache.put(3, 3);
            cache.get(2);
            cache.put(4, 4);
            cache.put(5, 5);

        }
    }


}
