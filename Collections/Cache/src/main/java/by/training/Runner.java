package by.training;

import by.training.impl.ConcurrencyLFUCache;
import by.training.impl.ConcurrencyLRUCache;
import by.training.impl.LRUCache;
import by.training.interfaces.ICache;

public class Runner {
    private static final int CACHE_SIZE = 2;

    public static void main(String[] args) {
        ICache cache = new ConcurrencyLRUCache(CACHE_SIZE);
        someWork(cache);
        cache = new LRUCache(CACHE_SIZE, (float)0.8, true);
        someWork(cache);
        cache = new ConcurrencyLFUCache(CACHE_SIZE);
        someWork(cache);
    }

    public static void someWork(ICache cache) {
        System.out.println("\n++++++++++++" + cache.getClass() + "++++++++++++");
        cache.put(1, 1);
        cache.put(2, 2);
        cache.get(1);
        cache.get(1);
        cache.get(1);
        cache.get(1);
        cache.put(3, 3);
        cache.get(2);
        cache.put(4, 4);
        System.out.println(cache.getMap().toString());
    }

}
