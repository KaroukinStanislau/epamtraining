package by.training.impl;

import by.training.interfaces.ICache;

import java.util.LinkedHashMap;
import java.util.Map;

public class LRUCache<K, V> implements ICache<K, V> {
    private final Map<K, V> map;

    public LRUCache(int capacity, float loadFactor, boolean accessOrder) {
        this.map = createLincedHashMap(capacity, loadFactor, accessOrder);
    }

    private Map<K, V> createLincedHashMap(final int capacity, float loadFactor, boolean accessOrder) {
        return new LinkedHashMap<K, V>(capacity, loadFactor, accessOrder) {

            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return size() > capacity;
            }
        };
    }

    public Map<?, ?> getMap() {
        return map;
    }

    public void put(K key, V value) {
        map.put(key, value);
    }

    public V get(K key) {
        V temp = map.get(key);
        if (temp == null){
            System.out.println("Key: " + key + " not exists!");
            return null;
        }
        map.remove(key);
        map.put(key, temp);
        return temp;
    }
}
