package by.training.impl;

import by.training.interfaces.ICache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrencyLRUCache<K, V> implements ICache<K, V> {

    private final int size;
    private ConcurrentLinkedQueue<K> linkedQueue;
    private ConcurrentHashMap<K, V> map;

    public ConcurrencyLRUCache(final int size) {
        this.size = size;
        this.linkedQueue = new ConcurrentLinkedQueue<K>();
        this.map = new ConcurrentHashMap<K, V>(size);
    }

    public V get(final K key) {
        if(!map.containsKey(key)){
            System.out.println("Key: " + key + " not exists!");
            return null;
        }
        if (map.containsKey(key)) {
            linkedQueue.remove(key);
            linkedQueue.add(key);
        }
        return map.get(key);
    }

    public synchronized void put(final K key, final V value) {
        if (map.containsKey(key)) {
            linkedQueue.remove(key);
        }
        while (linkedQueue.size() >= size) {
            K oldestKey = linkedQueue.poll();
            if (oldestKey != null) {
                System.out.println("removed from ConcurrencyLRU {value=" + map.get(oldestKey)
                        + "}");
                map.remove(oldestKey);
            }
        }
        linkedQueue.add(key);
        map.put(key, value);
    }

    public Map<K, V> getMap() {
        return map;
    }
}
