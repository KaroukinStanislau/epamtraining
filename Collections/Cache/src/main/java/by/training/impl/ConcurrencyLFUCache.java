package by.training.impl;

import by.training.interfaces.ICache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrencyLFUCache<K, V> implements ICache<K, V> {
    private Map<K, Entry<V>> map;
    private int size;
    private int maxFrequency;

    public ConcurrencyLFUCache(final int size) {
        this.size = size;
        maxFrequency = size - 1;
        map = new ConcurrentHashMap<K, Entry<V>>(size);
    }

    public Map<?, ?> getMap() {
        return map;
    }

    public void put(K key, V value) {
        if (isFull()) {
            removeEldestEntry();
        }
        Entry<V> temp = new Entry<V>(value, 0);
        map.put(key, temp);

    }

    public V get(Object key) {
        if (!map.containsKey(key)) {
            System.out.println("Key: " + key + " not exists!");
            return null;
        }
        Entry<V> temp = map.get(key);
        if (temp.frequency < maxFrequency) {
            temp.frequency++;
        }
        return map.get(key).value;
    }

    private void removeEldestEntry() {
        Long minTime = Long.MAX_VALUE;
        int minFrequency = Integer.MAX_VALUE;
        K key = null;
        for (Map.Entry<K, Entry<V>> entry : map.entrySet()) {
            Entry<V> temp = entry.getValue();
            if (temp.frequency < minFrequency) {
                key = entry.getKey();
                minFrequency = temp.frequency;
            }
        }
        System.out.println("removed from LFU: " + map.get(key));
        map.remove(key);
    }

    private boolean isFull() {
        return map.size() == size;
    }

    private class Entry<V> {
        public V value;
        public int frequency;

        public Entry(V value, int frequency) {
            this.value = value;
            this.frequency = frequency;
        }

        @Override
        public String toString() {
            return "{" +
                    "value=" + value +
                    ", frequency=" + frequency +
                    "}";
        }
    }
}
